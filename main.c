/*
Introduction
In this assignment you will be writing a program to load a dictionary of 
passwords into an array, then search the array for entries. You will be using 
some of the concepts we have been learning about recently.
    Dynamically-allocated memory (malloc)
    Resizing arrays (realloc)
    Searching through an array (strcmp)
    Memory management and debugging (Valgrind)

Getting Started

You will be given some files to start with. 

    Open a browser and go to my repository at https://bitbucket.org/csci46/lab11 (Links to an external site.)
    Fork this repository to make a copy for yourself.
    Clone the repository into your Cloud 9 environment.

Let's look at some of the files that are in the directory.
dictionary.c — Contains functions for loading and searching a dictionary 
				of passwords.
dictionary.h — Header file for dictionary.c.
main.c — The driver program that loads the dictionary and prompts the user 
				for strings to search for.
Makefile — Used to build and test the program.
words1.txt, words2.txt, words3.txt — Files of assorted words to use for testing.


Specification
This program loads one of the word files into memory, then prompts the user to 
				search for words until DONE is entered.

The file dictionary.c contains the starter code. There are two functions defined:
				char ** loadDictionary(char *filename, int *size)
				This function loads the dictionary (a file of words, one word 
				per line) into an array of strings. The function returns a 
				pointer to the array and adjusts the value of size to match 
				the number of entries in the array.
Since the number of words is not known ahead of time, the function should use 
				dynamic memory (malloc and realloc) to expand the array to 
				accommodate all the words in the file.
			char * searchDictionary(char *target, char **dictionary, int size)
Search the given dictionary for a target string. Both a pointer to the 
				dictionary and its size (the number of valid entires) must be 
				passed in.
Returns the found string, or NULL if not found.

Compiling the Program

Simply type make in your terminal. It will compile the C files into object 
				files, then link them together.
Running the Program
Run the program with a dictionary file (such as words1.txt) as the only 
				command line argument.
				
What to Turn In
When you have finished, commit your changes using Git.
Push your local repository to BitBucket.
Be sure to add me as a user on your BitBucket repository so I can read it.
Submit the URL to your repository here.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "dictionary.h"


int main(int argc, char *argv[])
{
	if (argc < 1)
	{
		fprintf(stderr, "Must supply a dictionary filename\n");
		exit(1);
	}
	int i;
	int dlen;
	
	char **d = loadDictionary(argv[1], &dlen);
	printf("Dictionary loaded.\n");

	while(1)
	{
		char target[100];
		printf("Word to search for: ");
		fgets(target, 100, stdin);
		// Trim newline
		char *nl = strchr(target, '\n');
		if (nl) *nl = '\0';
		
		if (strcmp(target, "DONE") == 0) break;
		
		char *found = searchDictionary(target, d, dlen);
		if (found)
			printf("Found: %s\n", found);
		else
			printf("Not found!\n");
	}
}

