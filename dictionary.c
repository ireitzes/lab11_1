/*
CSCI0046
Ilan Reitzes
Lab 11
Introduction
In this assignment you will be writing a program to load a dictionary of 
passwords into an array, then search the array for entries. You will be using 
some of the concepts we have been learning about recently.
    Dynamically-allocated memory (malloc)
    Resizing arrays (realloc)
    Searching through an array (strcmp)
    Memory management and debugging (Valgrind)

dictionary.c — Contains functions for loading and searching a dictionary 
				of passwords.
dictionary.h — Header file for dictionary.c.
main.c — The driver program that loads the dictionary and prompts the user 
				for strings to search for.
Makefile — Used to build and test the program.
words1.txt, words2.txt, words3.txt — Files of assorted words to use for testing.

Specification
This program loads one of the word files into memory, then prompts the user to 
				search for words until DONE is entered.

The file dictionary.c contains the starter code. There are two functions defined:
				char ** loadDictionary(char *filename, int *size)
				This function loads the dictionary (a file of words, one word 
				per line) into an array of strings. The function returns a 
				pointer to the array and adjusts the value of size to match 
				the number of entries in the array.
Since the number of words is not known ahead of time, the function should use 
				dynamic memory (malloc and realloc) to expand the array to 
				accommodate all the words in the file.
			char * searchDictionary(char *target, char **dictionary, int size)
Search the given dictionary for a target string. Both a pointer to the 
				dictionary and its size (the number of valid entires) must be 
				passed in.
Returns the found string, or NULL if not found.

Compiling the Program

Simply type make in your terminal. It will compile the C files into object 
				files, then link them together.
Running the Program
Run the program with a dictionary file (such as words1.txt) as the only 
				command line argument.
				
What to Turn In
When you have finished, commit your changes using Git.
Push your local repository to BitBucket.
Be sure to add me as a user on your BitBucket repository so I can read it.
Submit the URL to your repository here.

*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "dictionary.h"

char a[20], b[20], c[100], d[20];
    int entries = 0;                  // entries set to 1 o bypass file name
    int v = 1;                      // holder for calculation
    int x;                          // holder for strcmp
//    printf("\n");

// Load the dictionary of words from the given filename.
// Return a pointer to the array of strings.
// Sets the value of size to be the number of valid
// entries in the array (not the total array length).

char ** loadDictionary(char *filename, int *size)
{

	//char a[20], b[20], c[100];
	FILE *in = fopen(filename, "r");
	if (!in)
	{
	    perror("Can't open dictionary file");
	    exit(1);
	}
// Set variables and allocate memory	
	int arrayLength = 10;
	char ** arr = malloc(arrayLength * sizeof(char *));
	char *words = malloc(arrayLength * sizeof(char *));
	int entries = 0;
	char line[20];
	char lineEntry[20];// <------

// while loop	
	while(fgets(line, 20, in) != NULL)
	{
		char *words = malloc(arrayLength * sizeof(char *));
//  Find \n and replace with /0
		char *nl = strchr(line, '\n');      // Find new line in array
		if (nl != NULL)                 // if not NULL
			{
			    *nl = '\0';             	// set null
			}
		entries++;
//  reallocate the array memory as needed				
		if (entries > arrayLength)
		{
			arrayLength += 10;
			arr = realloc(arr, (arrayLength+10) * sizeof(*arr));
		}  // End if
		
		strcpy(words,line); 	// lines --> words
		arr[entries] = words;	// words --> array

	}   // End while
	
	*size = entries;  // set sizes to the total
	return arr;   // return array
	free(arr);
	return NULL;
}

char * searchDictionary(char *target, char **dictionary, int size)
{
//	Set variables
	char line[20];
	int i = 1;
//    int z = 0;
    if (dictionary == NULL) return NULL;//	Check for dictionary
 	for (i = 1; i < size+1; i++)		//	Start loop
	{
		strcpy(line,dictionary[i]);		// copy dictionary element to line
 		char *nl = strchr(line, '\n');	// Find "new line" in line
 		if (nl != NULL)             	// if not NULL
 		{
 			*nl = '\0';                 // set null
 		}

	    if (strcmp(target, line) == 0)	// if string compare = 0
	    {								// then items match
	        return dictionary[i];		// return the matching dictionary entry
	    }
	}
	return NULL;
}